import requests

url = "https://hotels4.p.rapidapi.com/properties/list"

querystring = {"destinationId":"115902","pageNumber":"1","pageSize":"25","checkIn":"2020-01-08","checkOut":"2020-01-15","adults1":"1","sortOrder":"PRICE","locale":"en_US","currency":"USD"}

headers = {
    'x-rapidapi-host': "hotels4.p.rapidapi.com",
    'x-rapidapi-key': "b22fae68cdmsh2977f21b076aaebp1e66d4jsneec1de699d74"
    }

response = requests.request("GET", url, headers=headers, params=querystring)

print(response.text)